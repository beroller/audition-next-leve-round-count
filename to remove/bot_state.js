/*
 * equivalent to: CREATE TABLE bot_state(
 * name             string
 * state            enum
 * status           enum
 * );
 */
const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory');

class BotState extends Model { }

BotState.init({
    name: Sequelize.STRING,
    state: {
        type: Sequelize.ENUM,
        values: ["PLAYING", "STREAMING", "LISTENING", "WATCHING", "CUSTOM_STATUS", "COMPETING"],
    },
    status: {
        type: Sequelize.ENUM,
        values: ["online", "idle", "dnd", "invisible"],
    },
    timestamps: true,
    createdAt: true,
    updatedAt: 'updateTimestamp'
}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'BotState' // We need to choose the model name
});
module.exports = BotState;