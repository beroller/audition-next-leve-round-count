/*
 * equivalent to: CREATE TABLE prefix(
 * guild_id         string
 * guild_prefix     string
 * guidl_name       string
 * guild_owner      string
 * );
 */
const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory');

class Prefix extends Model { }

Prefix.init({
    guild_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    guild_prefix: Sequelize.STRING,
    guild_name: {
        type: Sequelize.STRING,
        unique: true,
    },
    guild_owner: Sequelize.STRING,
    timestamps: true,
    createdAt: true,
    updatedAt: 'updateTimestamp'
}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'Prefix' // We need to choose the model name
});
module.exports = Prefix;