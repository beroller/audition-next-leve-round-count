const { MessageEmbed } = require('discord.js');
const csv = require('fast-csv');
const fs = require('fs');
const Rdata = require('../model/rounds_data')
const fetch = require('node-fetch');

module.exports = {
    name: 'rounds',
    description: 'Display game rounds.',
    execute(message, args) {
        var data = [];
        var isFound = true;
        var isChannelonlyReply = false;
        var isExist = false;
        var jsonDATA = []
        const amount = parseInt(args.length);
        message.delete({ timeout: 50, reason: 'Line Command Remove' });

        const ChannelonlyReply = async () => {
            await fetch(`http://localhost:3000/channel?id=${message.guild.id}`, {
                method: 'GET',
            })
                .then(response => response.text())
                .then((result) => {
                    isExist = Object.keys(JSON.parse(result)).length !== 0 || Object.keys(JSON.parse(result)).length !== -1
                    isChannelonlyReply = JSON.parse(result)[0].channelID == message.channel.id
                    jsonDATA = JSON.parse(result)[0]
                })
                .catch(error => console.log('error get data', error));

            if (isExist) {
                if (isChannelonlyReply) {
                    if (amount == 0) {
                        const embeds = new MessageEmbed()
                        embeds.setTitle('Rounds Command')
                        embeds.setDescription(`command:\n?rounds [IGN] or ?rounds [IGN] [IGN] multiple ign's.\n\ne.g. ?rounds AuditionPH or ?rounds AuditionPH GMFaith`, message.author)
                        embeds.setColor(0xff0080)
                        return message.reply(embeds);
                    }


                    const _data = JSON.parse(fs.readFileSync('./commands/rounds.json', 'utf-8').toString());
                    const rdata = new Rdata(_data);

                    fs.createReadStream('rounds.csv')
                        .pipe(csv.parse({ headers: false }))
                        .on('error', error => console.error(error))
                        .on('data', row => {
                            for (const player_ign of args) {
                                if (row[0] == player_ign) {
                                    data.push({ "IGN": row[0], "round": row[1] });
                                    isFound = false
                                    break;
                                }
                            }
                        })
                        .on('end', () => {
                            console.log(data);
                            if (isFound) {
                                const embed = new MessageEmbed()
                                    .setTitle('Opss..')
                                    .setColor(0xff0000)
                                    .setDescription(`Sorry there's no data on the Database`, message.author)
                                    .setAuthor(rdata.author, rdata.image)
                                message.channel.send(embed);
                            } else {
                                const embeds = new MessageEmbed()
                                embeds.setTitle(rdata.title)
                                embeds.setDescription(rdata.description, message.author)
                                embeds.setURL(rdata.anl_weblink)
                                embeds.setColor(0xff0080)
                                embeds.setImage(rdata.image)
                                embeds.setFooter(rdata.footer)
                                data.forEach(data => {
                                    if (amount == 1) {
                                        embeds.addField("IGN", data.IGN, true)
                                        embeds.addField("Rounds", data.round, true)
                                    } else {
                                        embeds.addField(data.IGN, data.round, true)
                                    }
                                });
                                message.channel.send(embeds);
                            }
                        })
                        .on('error', (error) => {
                            console.log(error)
                        });


                    var serverinfo = {
                        g_name: message.guild.name,
                        g_id: message.guild.id,
                        g_systemChannelID: message.guild.systemChannelID,
                        g_ownerID: message.guild.ownerID,
                        g_joinedTimestamp: message.guild.joinedTimestam,
                        c_info: message.guild.channels.cache
                    };

                    fs.writeFile(`./db/${message.guild.name}.json`, JSON.stringify(serverinfo, null, 4), (err) => {
                        if (err) throw err;
                    });
                } else {
                    const embed = new MessageEmbed()
                        .setTitle('Hey!!')
                        .setColor(0xff0000)
                        .setDescription(`You must use this command on the <#${jsonDATA.channelID}> channel.`, "Admin")
                    message.channel.send(embed);
                    message.delete({ timeout: 10000, reason: 'Line Command Remove' });
                }
            } else {
                const embed = new MessageEmbed()
                    .setTitle('Hey!!')
                    .setColor(0xff0000)
                    .setDescription("You must set a channel.\nUse `?set channel` to set the bot only to reply to a specific channel.", "Admin")
                message.channel.send(embed);
                message.delete({ timeout: 10000, reason: 'Line Command Remove' });
            }
        }
        ChannelonlyReply()
    },
};