const request = require('request');
const fs = require('fs');

module.exports = {
    name: 'update-rounds',
    description: 'Update method.',
    execute(message, args) {
        let file = fs.createWriteStream(`rounds.csv`);
        const _data = JSON.parse(fs.readFileSync('./commands/rounds.json', 'utf-8').toString());
        message.delete({ timeout: 50, reason: 'Line Command Remove' });
        message.reply(`\nSheet_ID: ${_data.sheet_ID}\nDownloading File...\nwill ping you once the download is finished.`);
        const hooks = async () => {
           await new Promise((resolve, reject) => {
                console.log(`sheet_ID: ${_data.sheet_ID}`);
                console.log(`Downloading File...`);
                console.log(`sending request data...`);
                let stream = request({
                    /* Here you should specify the exact link to the file you are trying to download */
                    /* https://docs.google.com/spreadsheets/d/1tqvElewz2djIr5vtblHiMbmjOxxmwmwg_QFg6Vhm4H0/gviz/tq?tqx=out:csv&tq& */
                    uri: `https://docs.google.com/spreadsheets/d/${_data.sheet_ID}/gviz/tq?tqx=out:csv&tq&`,
                    headers: {
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                        'Accept-Encoding': 'gzip, deflate, br',
                        'Accept-Language': 'en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3',
                        'Cache-Control': 'max-age=0',
                        'Connection': 'keep-alive',
                        'Upgrade-Insecure-Requests': '1',
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'
                    },
                    /* GZIP true for most of the websites now, disable it if you don't need it */
                    gzip: true
                }).pipe(file)
                    .on('finish', () => {
                        console.log(`The file is finished downloading.`);
                        message.reply(`Sheet updated!! :D`);
                        resolve();
                    })
                    .on('error', (error) => {
                        reject(error);
                    })
            }).catch(error => {
                console.log(`Something happened: ${error}`);
            });
        };
        hooks();
    },
};