const fs = require('fs');
const isValid = require('valid-url');
const Rdata = require('../model/rounds_data');
const { MessageEmbed } = require('discord.js');
module.exports = {
    name: 'rounds-config',
    description: 'Set rounds.json data',
    execute(message, args) {
        const _args = args.slice(1);
        const _data = JSON.parse(fs.readFileSync('./commands/rounds.json', 'utf-8').toString());
        var cat = args[0];
        var val = _args.join(' ');
        const rdata = new Rdata(_data);

        message.delete({ timeout: 50, reason: 'Line Command Remove' });

        if (!message.guild.member(message.author).hasPermission('MANAGE_MESSAGES')) {
            return message.channel.reply(":x: You don't have permissions. 😔 ):x:")
        }       

        if (_args.length == 0) {
            const embeds = new MessageEmbed()
            embeds.setTitle('Rounds Configuration')
            embeds.setDescription('command:\n?rounds-config [Category] [value]\n\ne.g. ?rounds-config title hello world', message.author)
            embeds.setColor(0xff0080)
            embeds.addFields(
                { name: '\u200B', value: 'CATEGORY:' },
                { name: 'Format: TEXT', value: 'title\nauthor\ndescription\nfooter\nsheet'.toLocaleUpperCase(), inline: true },
                { name: 'Format: URL', value: 'logo\nimage'.toLocaleUpperCase(), inline: true },
                { name: '\u200B', value: 'VALUES' },
                { name: 'title', value: (rdata.title != '')? rdata.title : '-' },
                { name: 'author', value: (rdata.author != '')? rdata.author : '-' },
                { name: 'link', value: (rdata.anl_weblink != '')? rdata.anl_weblink : '-' },
                { name: 'description', value: (rdata.description != '')? rdata.description : '-' },
                { name: 'logo', value: (rdata.logo != '')? rdata.logo : '-' },
                { name: 'image', value: (rdata.image != '')? rdata.image : '-' },
                { name: 'footer', value: (rdata.footer != '')? rdata.footer : '-' },
                { name: 'sheet', value: (rdata.sheet_ID != '')? rdata.sheet_ID : '-' },
            )
            return message.reply(embeds);
        }
        
        switch (cat.toLowerCase()) {
            case 'title':
                if(!isValid.isUri(val)) {
                    rdata.set_title(val);
                    isEdit = true;
                } else { return message.reply('Not a valid TEXT, Please try again') }
                break;
            case 'author':
                if(!isValid.isUri(val)) {
                    rdata.set_author(val);
                    isEdit = true;
                } else { return message.reply('Not a valid TEXT, Please try again') }
                break;
            case 'sheet':
                if(!isValid.isUri(val)) { 
                    rdata.set_anl_weblink(`https://docs.google.com/spreadsheets/d/${val}/edit?usp=drivesdk`);
                    rdata.set_sheet_ID(val);
                    isEdit = true;
                } else { return message.reply('Not a Valid URL, Please try again') }
                break;
            case 'description':
                if(!isValid.isUri(val)) {
                    rdata.set_description(val);
                    isEdit = true;
                } else { return message.reply('Not a valid TEXT, Please try again') }
                break;
            case 'logo':
                if(isValid.isUri(val)) { 
                    rdata.set_logo(val);
                    isEdit = true;
                } else { return message.reply('Not a Valid URL, Please try again') }
                break;
            case 'image':
                if(isValid.isUri(val)) { 
                    rdata.set_image(val);
                    isEdit = true;
                } else { return message.reply('Not a Valid URL, Please try again') }
                break;
            case 'footer':
                if(!isValid.isUri(val)) {
                    rdata.set_footer(val);
                    isEdit = true;
                } else { return message.reply('Not a valid TEXT, Please try again') }
                break;
        }

        if(isEdit) {
            fs.writeFile(`./commands/rounds.json`, JSON.stringify(rdata.getData(), null, 4), (err) => {
                if (err) throw err;
            });
            message.reply(`${cat.toLocaleUpperCase()} data change complete.`);
        }
    },
};
