const fs = require('fs');

module.exports = {
    name: 'clean-rounds',
    description: 'remove rounds data',
    execute(message, args) {
        message.delete({ timeout: 50, reason: 'Line Command Remove' });
        
        if (!message.guild.member(message.author).hasPermission('MANAGE_MESSAGES')) {
            return message.channel.send(":x: You don't have permissions. :x:")
        }

        fs.writeFile(`./rounds.csv`, '', (err) => {
            if (err) throw err;
        });
        console.log(`rounds.csv clean`);
        message.reply(`database clean.`);
    }
}