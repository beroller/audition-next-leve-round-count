const { MessageEmbed } = require('discord.js');
const fs = require('fs');

module.exports = {
	name: 'speak',
	description: 'bot speaking',
	execute(message, args) {
		const _args = args.slice(0);
		message.delete({ timeout: 50, reason: 'Line Command Remove' });
		const _data = JSON.parse(fs.readFileSync('./commands/rounds.json', 'utf-8').toString());
		// console.log(_args)
		// console.log(typeof Number(_args[0]))
		// console.log(typeof _args[1])
		// console.log(typeof _args[2])

		if (!message.guild.member(message.author).hasPermission('MANAGE_MESSAGES')) {
			return message.channel.send(":x: You don't have permissions. :x:")
		}

		const channel = message.guild.channels.cache.get(message.channel.id/*message.guild.channels.cache.first().id*/);

		const hooks = async () => {
			const webhooks = await channel.fetchWebhooks();
			const webhook = webhooks.first();

			console.log(embed);

			if(webhook == undefined) {
				channel.createWebhook('ANL Rounds', {
					avatar: avatar,
				})
				.then(webhook => console.log(webhook))
				.catch(console.error);
			} else {
				await webhook.send(_args.join().replace(',', ' '), {
					username: 'Admin',
					avatarURL: _data.logo,
					embeds: [embed],
				});
			}
		}
		hooks();
	},
};