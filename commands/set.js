const { MessageEmbed } = require('discord.js');
const fetch = require('node-fetch');

module.exports = {
    name: 'set',
    description: 'set options.',
    execute(message, args) {
        var isExist = false;
        var isNochange = false;
        // json-server --watch db.json
        const amount = parseInt(args.length)
        message.delete({ timeout: 50, reason: 'Line Command Remove' });

        if (!message.guild.member(message.author).hasPermission('MANAGE_MESSAGES')) {
            return message.channel.send(":x: You don't have permissions. :x:")
        }

        if (amount < 2) {
            switch (args[0].toLowerCase()) {
                case "channel":
                    // check sercer exist . 
                    const setChannel = async () => {

                        await fetch(`http://localhost:3000/channel/${message.guild.id}`, {
                            method: 'GET',
                        })
                            .then(response => response.text())
                            .then((result) => {
                                isExist = Object.keys(JSON.parse(result)).length !== 0 || Object.keys(JSON.parse(result)).length !== -1
                                isNochange = JSON.parse(result).channelID == message.channel.id
                            })
                            .catch(error => console.log('error Get data', error));

                        if (!isExist) {
                            const raw = JSON.stringify({
                                "name": message.channel.name,
                                "channelID": message.channel.id,
                                "id": message.guild.id
                            });

                            const requestOptions = {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json"
                                },
                                body: raw
                            };
                            await fetch(`http://localhost:3000/channel`, requestOptions)
                                .then(res => res.json())
                                .catch((err) => {
                                    const embed = new MessageEmbed()
                                        .setTitle('Opss..')
                                        .setColor(0xff0000)
                                        .setDescription(`Sorry there was an error.`, "Admin")
                                    message.channel.send(embed);
                                    message.delete({ timeout: 10000, reason: 'Line Command Remove' });
                                });
                             const embed = new MessageEmbed()
                                .setColor(0x00ff00)
                                .setDescription(`Channel added.`, "Admin")
                            message.channel.send(embed);
                            message.delete({ timeout: 10000, reason: 'Line Command Remove' });
                        } else {
                            if (!isNochange) {
                                const raw = JSON.stringify({
                                    "name": message.channel.name,
                                    "channelID": message.channel.id
                                });

                                const requestOptions = {
                                    method: 'PATCH',
                                    headers: {
                                        "Content-Type": "application/json"
                                    },
                                    body: raw
                                };
                                await fetch(`http://localhost:3000/channel/${message.guild.id}`, requestOptions)
                                    .then(res => res.json())
                                    .catch((err) => {
                                        const embed = new MessageEmbed()
                                            .setTitle('Opss..')
                                            .setColor(0x00ff00)
                                            .setDescription(`Error setting a channel`, "Admin")
                                        message.channel.send(embed);
                                        message.delete({ timeout: 10000, reason: 'Line Command Remove' });
                                    });

                                const embed = new MessageEmbed()
                                    .setColor(0x00ff00)
                                    .setDescription(`Channel set update.`, "Admin")
                                message.channel.send(embed);
                                message.delete({ timeout: 10000, reason: 'Line Command Remove' });
                            } else {
                                const embed = new MessageEmbed()
                                    .setColor(0xff0000)
                                    .setDescription(`Already set this channel.`, "Admin")
                                message.channel.send(embed);
                                message.delete({ timeout: 10000, reason: 'Line Command Remove' });
                            }
                        }
                    };
                    setChannel();
                    break;
            }
        } else {
            console.log("else")
        }
    },
};