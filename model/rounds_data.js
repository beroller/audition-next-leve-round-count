class json_data {
    constructor(data) {
        this.title = data.title;
        this.author = data.author;
        this.anl_weblink = data.anl_weblink;
        this.description = data.description;
        this.logo = data.logo;
        this.image = data.image;
        this.footer = data.footer;
        this.sheet_ID = data.sheet_ID;
    }

    set_title(val) {
        this.title = val;
    }

    set_author(val) {
        this.author = val;
    }

    set_anl_weblink(val) {
        this.anl_weblink = val;
    }

    set_description(val) {
        this.description = val;
    }

    set_logo(val) {
        this.logo = val;
    }

    set_image(val) {
        this.image = val;
    }

    set_footer(val) {
        this.footer = val;
    }

    set_sheet_ID(val) {
        this.sheet_ID = val;
    }

    getData() {
        var data = {
            title : this.title,
            author : this.author,
            anl_weblink : this.anl_weblink,
            description : this.description,
            logo : this.logo,
            image : this.image,
            footer : this.footer,
            sheet_ID: this.sheet_ID
        }
        return data;
    }
}
module.exports = json_data;